# This is a sample Python script.

# Press ⌥⇧X to execute it or replace it with your code.
# Press Double ⇧ to search everywhere for classes, files, tool windows, actions, and settings.
import matplotlib.pyplot as plt
import numpy as np


def central_diff_operator(n_nodes):
    op = np.eye(n_nodes, k=1)
    op = op - np.eye(n_nodes, k=-1)
    return op


def fy(A, w):
    return A / w


def steady_stvenant(n_nodes, Ao, Qo, Aup, Adown, Qup, Qdown, dx, man_n, w, So, tol=1e-6):
    g = 9.81
    G = central_diff_operator(n_nodes)
    Q = Qo
    A = Ao
    BC_Q = np.zeros(n_nodes)
    BC_A = np.zeros(n_nodes)
    BC_A[0] = -Aup
    BC_A[-1] = Adown
    BC_Q[0] = -Qup
    BC_Q[-1] = Qdown

    dQsq = 1
    while np.linalg.norm(dQsq) > tol:
        D = (np.dot(G, Q) + BC_Q) / Q

        lhs_A = G + np.diag(D)
        rhs_A = - (np.dot(lhs_A, A) + BC_A)

        dA = np.linalg.solve(lhs_A, rhs_A)
        A += dA

        y = fy(A, w)
        AA = 1 / A
        BB = 2 * g * dx * man_n ** 2 / (A * y ** (4 / 3))
        DD = 2 * g * dx * A * So - g * A * (np.dot(G, y) + fy(BC_A, w))

        lhs_Q = np.dot(G, np.diag(AA)) + np.diag(BB)
        rhs_Q = DD - (np.dot(lhs_Q, Q*Q) + (BC_Q*BC_Q) * (np.where(BC_A != 0, 1 / BC_A, 1)))

        dQsq = np.linalg.solve(lhs_Q, rhs_Q)
        Q = np.sqrt(Q**2 + dQsq)

    return Q, A


if __name__ == '__main__':

    n = 10
    S0 = np.ones(n) * 0.001
    dx = np.ones(n) * 90
    manning_n = np.ones(n) * 0.05
    width = 0.2


    #Q0 = np.ones(n)
    Qup = 1.0
    Qdown = 2.0

    Aup = 1
    Adown = 2
    A0 = np.ones(n) * 0.5 * (Aup + Adown)
    Q0 = np.arange(1, n + 1) * (Qdown - Qup) / (n+1) + Qup
    steady_stvenant(n, A0, Q0, Aup, Adown, Qup, Qdown, dx, manning_n, width, S0)
